import request from '@/utils/config'


export function login() {
  return request({
    url:  '/api/login',
    method: 'get'
  })
}

export function course() {
  return request({
    url:  '/api/course',
    method: 'GET'
  })
}
export function courseSubmit(parm) {
  return request({
    url:  '/api/course',
    method: 'post',
    data: parm
  })
}
export function courseEdit(id, parm) {
  return request({
    url:  '/api/course/' + id,
    method: 'put',
    data: parm
  })
}
export function courseDel(parm) {
  return request({
    url:  '/api/course/' + parm,
    method: 'DELETE'
  })
}