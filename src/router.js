import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [{ path: '/', redirect: '/home' },
    {
      path: '/login',
      name: 'login',
      component: () => import('@/views/login.vue')
    },{
      path: '/home',
      name: 'home',
      component: () => import('@/views/Home.vue'),
      children: [
        { path: '/home', redirect: 'homePage' },
        {
          path: 'homePage',
          name: '首页',
          component: () => import('@/views/homePage/index.vue')
        },{
          path: 'anti-networkFraud',
          name: '反网络欺诈',
          component: () => import('@/views/anti-networkFraud/index.vue')
        },{
          path: 'treasureBox',
          name: '百宝箱',
          component: () => import('@/views/treasureBox/index.vue')
        },
        {
          path: 'giganticNet',
          name: '天罗地网',
          component: () => import('@/views/giganticNet/index.vue'),
          children: [
            { path: '/home/giganticNet', redirect: 'controlPlan' },
            {
              path: 'controlPlan',
              name: '管控计划',
              component: () => import('@/views/giganticNet/controlPlan.vue')
            }, {
              path: 'eventAlarm',
              name: '事件与报警',
              component: () => import('@/views/giganticNet/eventAlarm.vue')
            }
          ]
        },
        {
          path: 'godCatchingCompass',
          name: '神捕罗盘',
          component: () => import('@/views/godCatchingCompass/index.vue')
        },
        {
          path: 'users',
          name: '个人中心',
          component: () => import('@/views/users/index.vue'),
          children: [
            { path: '/home/users', redirect: 'monitoringPersonnelParameter' },
            {
              path: 'monitoringPersonnelParameter',
              name: '监控人员台账',
              component: () => import('@/views/users/monitoringPersonnelParameter.vue')
            },{
              path: 'kuaiBuLing',
              name: '快捕令',
              component: () => import('@/views/users/kuaiBuLing.vue')
            },{
              path: 'userInfo',
              name: '个人资料',
              component: () => import('@/views/users/userInfo.vue')
            },{
              path: 'securitySettings',
              name: '安全设置',
              component: () => import('@/views/users/securitySettings.vue')
            }
          ]
        }
      ]
    }
  ]
})
