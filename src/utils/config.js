import axios from 'axios'
import router from '@/router'
import {
  Message
} from 'element-ui'
import {
  getToken
} from '@/utils/auth'

// create an axios instance
const service = axios.create({
  timeout: 20000 // request timeout
})
const timeEl = 1000

// request拦截器
service.interceptors.request.use(config => {
  console.log(config)
  /*  debugger; */
  // Do something before request is sent
  if (getToken()) {
    config.headers.Authorization = getToken(); // 让每个请求携带token--['X-Token']为自定义key 请根据实际情况自行修改
  } else {
    config.headers.Authorization = 'Basic YXBwOmFwcA=='; // 增加客户端认证
  }
  config.headers['x-tenant-flag'] = '1';
  return config
}, error => {
  // Do something with request error
  console.log(error) // for debug
  Promise.reject(error)
})

// respone拦截器
service.interceptors.response.use(
    response => {

      const res = response.data
      if (response.status !== 200 && res.status !== 200) {
        Message({
          message: res.message,
          type: "success",
          duration: timeEl,
        })
        /*  alert(121) */
      } else {
        if (response.data.status === 101011) {
          router.replace({  // 未登录时
            path: '/login',
            query: {
              idss: 1
            }
          })
        }
        return response.data
      }
    },
    error => {
      const response = error.response

      /* alert(response.status) */
      if (response === undefined) {
        this.loading = false
        Message({
          message: '服务请求超时！',
          type: "error",
          duration: timeEl,
        })
        return Promise.reject(error)
      }
      if (response.status === 404) {
        Message({
          message: '服务请求超时！',
          type: "error",
          duration: timeEl,
        })
        return Promise.reject(error)
      }
      if (response.status === 304) {
        Message({
          message: '此资源未找到！',
          type: "error",
          duration: timeEl,
        })
        return Promise.reject(error)
      }
      if (response.status === 500) {
        Message({
          message: '系统出了点儿问题！',
          type: "error",
          duration: timeEl,
        })
        return Promise.reject(error)
      }
      if (response.status === 400) {
        Message({
          message: '账号或密码错误！',
          type: "error",
          duration: timeEl,
        })
        return Promise.reject(error)
      }
      if (response.status === 504) {
        Message({
          message: '后端服务异常，请联系管理员！',
          type: "error",
          duration: timeEl,
        })
        return Promise.reject(error)
      }
      if (response.status === 401) {
        if (response.data.error == 'invalid_token') {
          Message({
            message: '用户身份过期，请重新登录！',
            type: "error",
            duration: timeEl,
          })
          router.replace({  // 未登录时
            path: '/login',
            query: {redirect: '/home/jpbt/homePage'}
          })
        } else if (response.data.error == 'Unauthorized') {
          Message({
            message: '用户没有权限！',
            type: "error",
            duration: timeEl,
          })
          router.replace({  // 未登录时
            path: '/login',
            query: {redirect: '/home/jpbt/homePage'}
          })
        } else if (response.data.error_description + '' === '101012') {
          router.replace({  // 未登录时
            path: '/login',
            query: {
              idss: 2
            }
          })
        }
      }
      return Promise.reject(error)
    }
)

export default service
