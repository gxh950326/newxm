import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store/index'
import Cookies from 'js-cookie'
import '@/assets/publicCss.css' /*引入公共样式*/
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css'

Vue.use(ElementUI)

import VeeValidate, {
  Validator
} from 'vee-validate'
Vue.use(VeeValidate, {
  fieldsBagName: 'vee-fields'
})

// 汉化表单验证
import zhCN from 'vee-validate/dist/locale/zh_CN'
Validator.localize('zh_CN',
    zhCN)


Vue.config.productionTip = false


// 设置路由拦截
router.beforeEach((to, from, next) => {
  let name = Cookies.get('name') || store.state.name
  // 如果cookie没有过期或者store中有name值,则允许访问直接通过。否则就让用户登录
  if (name) {
    store.commit('loginIn', name)
    next()
  } else {
    if (to.path == '/login') {
      next()
    } else {
      next({
        name: 'login'
      })
      store.commit('loginOut')
    }
  }
})

router.afterEach(() => {})


new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
